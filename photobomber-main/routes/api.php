<?php

use App\Http\Controllers\AlbumCompilationWebhookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/webhooks/compilation', AlbumCompilationWebhookController::class);
Route::get('photo','App\Http\Controllers\UploadPhotoController@showPhotos');
Route::post('uploadimage','App\Http\Controllers\UploadPhotoController@uploadImage');
Route::post('newalbum','App\Http\Controllers\UploadPhotoController@newAlbum');
Route::post('updatealbum/{id}','App\Http\Controllers\UploadPhotoController@updateAlbum');
Route::get('album','App\Http\Controllers\UploadPhotoController@showAlbums');
Route::get('getalbum/{id}','App\Http\Controllers\UploadPhotoController@getAlbum');
Route::post('addphotoalbum/{id}','App\Http\Controllers\UploadPhotoController@addPhotoAlbum');
Route::post('deletephotoalbum/{id}','App\Http\Controllers\UploadPhotoController@deletePhotoAlbum');

