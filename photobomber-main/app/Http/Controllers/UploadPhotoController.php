<?php

namespace App\Http\Controllers;

use App\Models\Photo as Photo;
use App\Models\Album as Album;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;



class UploadPhotoController extends Controller
{
    public function __invoke(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $path = $request->file('photo')->store('photos');

        return $user->photos()->create(['path' => $path]);
    }

    public function uploadImage(Request $request)
    {
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'name' => 'string|max:40',
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
             

                $extension = $request->image->extension();
                $request->image->storeAs('/', $validated['name'],['disk' => 'images']);
                $url = $validated['name'];
                $Photo = Photo::create([
                   'user_id' => 1,
                    'path' => $url,
                ]);

                return response(Photo::find($Photo->id), 200);
            }

        }
        abort(500, 'Could not upload image :(');
    }

    public function showPhotos()
    {
        return Photo::all();
    }

    public function showAlbums()
    {
        return Album::with('photos')->get();;
    }

    public function getAlbum($id)
    {
        return response(Album::with('photos')->find($id), 200);

    }

    public function newAlbum(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
         ]);
         $data = $request->all();
          $result =Album::create($data);
         return response(Album::with('photos')->find($result->id));
    }

    public function updateAlbum(Request $request, $id)
    {  
        $data = $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
         ]);
        $myalbum = Album::findOrFail( $id);
        $data = $request->all();

        $myalbum->update($data);
        return response( Album::find( $myalbum->id),200);
    }
    public function addPhotoAlbum(Request $request, $id)
    {
    

        DB::table('album_photo')->insert([
            'album_id' => $id,
           'photo_id' => $request->photo
        ]);
         return response(Photo::find($request->photo),200);
    }
    public function deletePhotoAlbum(Request $request, $id)
    {
        DB::table('album_photo')->where('album_id', '=', $id)->where('photo_id', '=', $request->photo)->delete();
        return response(Photo::find($request->photo),200);
    }




}
